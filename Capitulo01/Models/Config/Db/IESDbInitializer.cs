﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Capitulo01.Models.Config.Db {
    public class IESDbInitializer {
        public static void Initialize(IESContext context) {
            context.Database.EnsureCreated();
            if (context.Departamentos.Any()) {
                return;
            }

            var departamentos = new Departamento[]
            {
                new Departamento{Nome = "Ciência da Computação"},
                new Departamento{Nome = "Sistemas de Informação"}
            };

            foreach (Departamento d in departamentos) {
                context.Add(d);
            }
            context.SaveChanges();
        }
    }
}
